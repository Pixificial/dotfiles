[colours]
default-background = #CC051519
xresources_background = ${xrdb:background:#CC051519}

default-foreground = #E2D8CD
xresources_foreground = ${xrdb:foreground:#E2D8CD}

default-red = #F8818E
xresources_red = ${xrdb:color1:#F8818E}

default-green = #92D3A2
xresources_green = ${xrdb:color2:#92D3A2}

default-blue = #8ED0CE
xresources_blue = ${xrdb:color4:#8ED0CE}

default-blue-2 = #39A7A2
xresources_blue2 = ${xrdb:color12:#39A7A2}

[bar/pixibar]
width = 100%
height = 16

background = ${colours.default-background:#CC051519}
format-background = ${colours.default-background:#CC051519}
foreground = ${colours.default-foreground:#E2D8CD}
format-foreground = ${colours.default-forforeund:#E2D8CD}

modules-left = bspwm
modules-center = philosophy
modules-right = memory cpu pulseaudio wireless-network wired-network battery date 

separator = " | "

font-0 = "Terminus:pixelsize=16;3"
font-1 = "Terminus:pixelsize=16;3"
font-2 = "Terminus:pixelsize=16;3"

[module/date]
type = internal/date
date = %a %Y-%m-%d %l:%M:%S %p %Z

[module/battery]
type = internal/battery

battery = BAT0
adapter = AC

full-at = 98
time-format = %H:%M

label-charging = BAT↑ %percentage%% %time%
label-charging-foreground = ${colours.default-green:#92D3A2}

label-discharging = BAT↓ %percentage%% %time%
label-discharging-foreground = ${colours.default-red:#F8818E}

label-full = BAT %percentage%%
label-full-foreground = ${colours.default-blue-2:#39A7A2}

[module/bspwm]
type = internal/bspwm

pin-workspaces = false

enable-click = false
enable-scroll = false

label-focused = %index%<
label-focused-foreground = ${colours.default-blue-2:#39A7A2}
label-focused-padding-right = 1

label-occupied = %index%.
label-occupied-foreground = ${colours.default-green:#92D3A2}
label-occupied-padding-right = 1

label-urgent = %index%!
label-urgent-foreground = ${colours.default-red:#F8818E}
label-urgent-padding-right = 1

label-empty = %index%
label-empty-padding-right = 2

[module/wired-network]
type = internal/network
interface = eth0

label-connected = ETH %downspeed%(↓) %upspeed%(↑)
label-connected-foreground = ${colours.default-green:#92D3A2}

label-disconnected = ETH Disconnected
label-disconnected-foreground = ${colours.default-red:#F8818E}

[module/wireless-network]
type = internal/network
interface = wlan0

label-connected = WLAN %downspeed%(↓) %upspeed%(↑)
label-connected-foreground = ${colours.default-green:#92D3A2}

label-disconnected = WLAN Disconnected
label-disconnected-foreground = ${colours.default-red:#F8818E}

[module/pulseaudio]
type = internal/pulseaudio

sink = alsa_output.usb-0600_USBFC1-A-00.analog-stereo

label-volume = AUD %percentage%% (%decibels%dB)
label-volume-foreground = ${colours.default-green:#92D3A2}

label-muted = AUD 0% (-INFdB)
label-muted-foreground = ${colours.default-red:#F8818E}

[module/cpu]
type = internal/cpu

label = CPU %percentage%%

[module/memory]
type = internal/memory

label = RAM %mb_used%+%gb_free%

[module/philosophy]
type = custom/text

content = Liberty. Privacy. Decentralisation.
content-background = ${colours.default-blue:#8ED0CE}
content-foreground = ${colours.default-background:#051519}
content-padding = 2
