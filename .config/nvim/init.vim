" General
syntax on
filetype plugin on
set nocompatible
set number relativenumber
set encoding=utf-8
set omnifunc=syntaxcomplete#Complete

" Visual
set tabstop=4
set shiftwidth=4
" Plug-ins
