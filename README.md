# Dotfiles
## About
This repository contains my personal software configurations and *nix desktop  
customisations.

## Licence
Dotfiles
Copyright (C) 2021 Abdullah Çırağ

My dotfiles are licenced under the Apache License, Version 2.0 (the "Licence").  
You may use my dotfiles only in compliance with the Licence. You may obtain a  
copy of the Licence in the LICENSE file included in the repository, or at

	http://www.apache.org/licenses/LICENSE-2.0
