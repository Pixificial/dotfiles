# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/pixificial/.zshrc'

autoload -Uz compinit promptinit
compinit
# End of lines added by compinstall
promptinit
prompt_pixiprompt_setup() {
	PROMPT='%B%F{4}> %n%f%b@%B%F{6}%m %b%f%~/ %B%f%#%b%f '
	RPROMPT='%b%f[%B%(?.%F{3}0.%F{1}1)%b%f]'
	PROMPT2='%B%F{4}%_	> %f%b'
}
prompt_themes+=( pixiprompt )
prompt pixiprompt
alias vi="nvim"
alias vim="nvim"
alias det="~/archive/unordered/projects/scripts/deattach.sh"
alias w3b="~/archive/unordered/projects/scripts/w3browse.sh"
